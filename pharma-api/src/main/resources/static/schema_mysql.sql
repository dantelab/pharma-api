DROP SCHEMA IF EXISTS Farmacia;
CREATE SCHEMA Farmacia;
USE Farmacia;
	
-- TABLAS COMPARTIDAS

CREATE TABLE IF NOT EXISTS Categoria (
    CategoriaId SMALLINT UNSIGNED AUTO_INCREMENT,
    Nombre VARCHAR(24) UNIQUE NOT NULL,
    PRIMARY KEY (CategoriaId)
);

CREATE TABLE IF NOT EXISTS Distrito (
    DistritoId TINYINT UNSIGNED AUTO_INCREMENT,
    Nombre VARCHAR(48) NOT NULL,
    PRIMARY KEY (DistritoId)
);

CREATE TABLE IF NOT EXISTS Cargo (
    CargoId TINYINT UNSIGNED AUTO_INCREMENT,
    Nombre VARCHAR(24) UNIQUE NOT NULL,
    PRIMARY KEY (CargoId)
);

CREATE TABLE IF NOT EXISTS Laboratorio (
    LaboratorioId SMALLINT UNSIGNED AUTO_INCREMENT,
    Nombre VARCHAR(48) UNIQUE NOT NULL,
    PRIMARY KEY (LaboratorioId)
);

-- TABLA DEPENDENCIA

CREATE TABLE IF NOT EXISTS Farmacia (
    FarmaciaId CHAR(11),
    Nombre VARCHAR(24) UNIQUE NOT NULL,
    Direccion VARCHAR(64) UNIQUE NOT NULL,
	Telefono CHAR(9) UNIQUE NOT NULL,
    DistritoId TINYINT UNSIGNED,
    Estado BIT DEFAULT 1,
    PRIMARY KEY (FarmaciaId),
    CONSTRAINT FK_Farmacia_Distrito FOREIGN KEY (DistritoId)
        REFERENCES Distrito (DistritoId)
);

CREATE TABLE IF NOT EXISTS Cliente (
    ClienteId MEDIUMINT UNSIGNED AUTO_INCREMENT,
    FarmaciaId CHAR(11) NOT NULL,
    Nombre VARCHAR(24) NOT NULL,
    ApellidoPaterno VARCHAR(24) NOT NULL,
    ApellidoMaterno VARCHAR(24) NOT NULL,
    Sexo ENUM('M', 'F') NOT NULL,    
    Dni CHAR(8) UNIQUE NOT NULL,
    Ruc CHAR(11) UNIQUE NULL,
    DistritoId TINYINT UNSIGNED,
    FechaRegistro DATETIME DEFAULT CURRENT_TIMESTAMP,
    Tipo ENUM('J', 'N') NOT NULL,
    PRIMARY KEY (ClienteId,FarmaciaId),
    CONSTRAINT FK_Cliente_Farmacia FOREIGN KEY (FarmaciaId)
        REFERENCES Farmacia (FarmaciaId),
    CONSTRAINT FK_Cliente_Distrito FOREIGN KEY (DistritoId)
        REFERENCES Distrito (DistritoId)
);

CREATE TABLE IF NOT EXISTS Usuario (
    UsuarioId SMALLINT UNSIGNED AUTO_INCREMENT,
    FarmaciaId CHAR(11) NOT NULL,
    Correo VARCHAR(20) UNIQUE NOT NULL,
    Clave VARCHAR(64) NOT NULL,
    Nombre VARCHAR(100) NOT NULL,
    CargoId TINYINT unsigned NOT NULL,
    Estado BIT DEFAULT 1 NOT NULL,
    PRIMARY KEY (UsuarioId,FarmaciaId),
    CONSTRAINT FK_Usuario_Farmacia FOREIGN KEY (FarmaciaId)
        REFERENCES Farmacia (FarmaciaId),    
    CONSTRAINT FK_Usuario_Cargo FOREIGN KEY (CargoId)
        REFERENCES Cargo (CargoId)
);

CREATE TABLE IF NOT EXISTS Producto (
    ProductoId INT UNSIGNED AUTO_INCREMENT,
    FarmaciaId CHAR(11) NOT NULL,
    Descripcion VARCHAR(100)  NOT NULL,
    Precio DECIMAL(5,2) NOT NULL,
    Stock MEDIUMINT UNSIGNED NOT NULL,
    CategoriaId SMALLINT UNSIGNED NOT NULL,
    LaboratorioId SMALLINT UNSIGNED NOT NULL,    
    PRIMARY KEY (ProductoId,FarmaciaId),
    CONSTRAINT FK_Producto_Farmacia FOREIGN KEY (FarmaciaId)
        REFERENCES Farmacia (FarmaciaId),
    INDEX FK_Producto_Farmacia_Idx (FarmaciaId ASC),
    CONSTRAINT FK_Producto_Categoria FOREIGN KEY (CategoriaId)
        REFERENCES Categoria (CategoriaId),
    INDEX FK_Producto_Categoria_Idx (CategoriaId ASC),
    CONSTRAINT FK_Producto_Laboratorio FOREIGN KEY (LaboratorioId)
        REFERENCES Laboratorio (LaboratorioId),
    INDEX FK_Producto_Laboratorio_Idx (LaboratorioId ASC) 
);
  
CREATE TABLE IF NOT EXISTS OrdenVenta (
    OrdenVentaId INT UNSIGNED AUTO_INCREMENT,
    FarmaciaId CHAR(11) NOT NULL,
    Serie CHAR(4) NOT NULL,
    Fecha DATETIME DEFAULT CURRENT_TIMESTAMP,
	UsuarioId SMALLINT UNSIGNED NOT NULL,    
    ClienteId MEDIUMINT UNSIGNED NULL,
    Condicion ENUM('PAGADO', 'ANULADO', 'PENDIENTE') NOT NULL,
    PRIMARY KEY (OrdenVentaId,FarmaciaId),
    
    CONSTRAINT FK_OrdenVenta_Farmacia FOREIGN KEY (FarmaciaId)
        REFERENCES Farmacia (FarmaciaId),
        
	CONSTRAINT FK_OrdenVenta_Usuario FOREIGN KEY (UsuarioId,FarmaciaId)
        REFERENCES Usuario (UsuarioId,FarmaciaId),
        
    CONSTRAINT FK_OrdenVenta_Cliente FOREIGN KEY (ClienteId,FarmaciaId)
        REFERENCES Cliente (ClienteId,FarmaciaId)
);

CREATE TABLE IF NOT EXISTS DetalleVenta (
    DetalleVentaId INT UNSIGNED,
	FarmaciaId CHAR(11) NOT NULL,
    ProductoId INT UNSIGNED NOT NULL,
    Cantidad INT UNSIGNED NOT NULL,
    Importe DECIMAL(7 , 2 ) NOT NULL,
    PRIMARY KEY (DetalleVentaId , ProductoId),
    
    CONSTRAINT FK_DetalleVenta_Farmacia FOREIGN KEY (FarmaciaId)
        REFERENCES Farmacia (FarmaciaId),
        
    CONSTRAINT FK_DetalleVenta_OrdenVenta FOREIGN KEY (DetalleVentaId,FarmaciaId)
        REFERENCES OrdenVenta (OrdenVentaId,FarmaciaId),
    
    CONSTRAINT FK_DetalleVenta_Producto FOREIGN KEY (ProductoId,FarmaciaId)
        REFERENCES Producto (ProductoId,FarmaciaId),
        
    INDEX FK_DetalleVenta_Producto_Idx (ProductoId ASC)
);
