package com.api.pharma.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.api.pharma.entity.Customer;
import com.api.pharma.service.CustomerService;
import com.api.pharma.util.Response;

@RestController
@RequestMapping("/api")

public class CustomerController {

	@Autowired
	private CustomerService customerService;

	// GET (all)

	@RequestMapping(value = "/CustomerList", method = RequestMethod.GET, headers = "Accept=application/json")
	public ResponseEntity<List<Customer>> getAllCustomers() {
		List<Customer> lista = new ArrayList<Customer>();
		lista = customerService.getAllCustomers();

		return new ResponseEntity<List<Customer>>(lista, HttpStatus.OK);
	}

	// GET (passing an id)

	@RequestMapping(value = "/Customer/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
	public ResponseEntity<Optional<Customer>> getCustomer(@PathVariable("id") long id) {
		Optional<Customer> emp = null;
		emp = customerService.getCustomer(id);

		return new ResponseEntity<Optional<Customer>>(emp, HttpStatus.OK);
	}

	// POST

	@RequestMapping(value = "/CustomerAdd", method = RequestMethod.POST, headers = "Accept=application/json")
	public ResponseEntity<Response> AddCustomer(@RequestBody Customer customer) {
		Response response = new Response();
		String success = "", error = "";
		try {

			if (customer.getNombre().equals("")) {
				error = "El campo nombre es requerido";
			} else {
				if (customer.getApellidoPaterno().equals(""))
					error = "El campo appelido paterno es requerido";
				else if (customer.getApellidoMaterno().equals(""))
					error = "El campo appelido materno es requerido";
				else if (customer.getSexo() == null)
					error = "El campo sexo es requerido";
				else if (customer.getDni().equals(""))
					error = "El campo Dni es requerido";
				else if (customer.getDni().length() != 8)
					error = "El Dni debe tener 8 digitos";
				else if (customer.getTipo() == null)
					error = "El campo tipo es requerido";
				else {
					customerService.addCustomer(customer);
					success = "Cliente insertado correctamente";
				}
			}

		} catch (Exception e) {
			error = e.getMessage();
			if (error.contains("DNI"))
				error = "El DNI ingresado ya existe para otro Cliente";
		}
		response.setSuccess(success);
		response.setError(error);
		System.out.println(success);
		System.out.println(error);

		return new ResponseEntity<Response>(response, HttpStatus.CREATED);
	}

	// PUT

	@CrossOrigin(origins = "http://localhost:8080")
	@RequestMapping(value = "/CustomerUpdate/{id}", method = RequestMethod.PUT, headers = "Accept=application/json")
	public ResponseEntity<Response> update(@PathVariable("id") long id, @RequestBody Customer customer) {
		Response response = new Response();
		String success = "", error = "";
		try {
			if (customer.getNombre().equals("")) {
				error = "El campo Nombre es requerido";
			} else {
				if (customer.getApellidoPaterno().equals(""))
					error = "El campo appelido paterno es requerido";
				else if (customer.getApellidoMaterno().equals(""))
					error = "El campo appelido materno es requerido";
				else if (customer.getSexo() == null)
					error = "El campo sexo es requerido";
				else if (customer.getDni().equals(""))
					error = "El campo Dni es requerido";
				else if (customer.getDni().length() != 8)
					error = "El Dni debe tener 8 digitos";
				else if (customer.getTipo() == null)
					error = "El campo tipo es requerido";
				else {
					customerService.updateCustomer(id, customer);
					success = "El cliente <strong>" + customer.getNombre() + " " + customer.getApellidoPaterno()
							+ "</strong> fue actualizado correctamente";
				}
			}
		} catch (Exception e) {
			error = e.getMessage();
			if (error.contains("DNI"))
				error = "El DNI ingresado ya existe para otro cliente";

		}
		response.setSuccess(success);
		response.setError(error);
		return new ResponseEntity<Response>(response, HttpStatus.OK);
	}

	// DELETE

	@CrossOrigin(origins = "http://localhost:8080")
	@RequestMapping(value = "/CustomerDelete/{id}", method = RequestMethod.DELETE, headers = "Accept=application/json")
	public ResponseEntity<Response> delete(@PathVariable("id") long id) {
		Response response = new Response();
		try {
			customerService.deleteCustomer(id);
			response.setSuccess("El cliente fue eliminado correctamente");
		} catch (Exception e) {
			response.setError("No se pudo eliminar al cliente, error: " + e.getMessage());
		}

		return new ResponseEntity<Response>(response, HttpStatus.OK);
	}

}
