package com.api.pharma.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.api.pharma.entity.Pharmacy;
import com.api.pharma.repository.PharmacyRepository;

@Service

public class PharmacyService {

	/*
	 * When Spring creates an instance of PharmacyService, the PharmacyRepository
	 * class is going to be injected.
	 */

	@Autowired
	private PharmacyRepository pharmacyRepository;

	// Method to list all pharmacies

	public List<Pharmacy> getAllPharmacies() {
		List<Pharmacy> pharmacies = new ArrayList<>();
		pharmacyRepository.findAll().forEach(pharmacies::add);
		return pharmacies;
	}

	// Method to add a new pharmacy

	public void addPharmacy(Pharmacy pharmacy) {
		pharmacyRepository.save(pharmacy);
	}

	// Method to find a pharmacy by id

	public Optional<Pharmacy> getPharmacy(long id) {
		return pharmacyRepository.findById(id);
	}

	// Method to update a pharmacy

	public void updatePharmacy(long id, Pharmacy Pharmacy) {
		pharmacyRepository.save(Pharmacy);
	}

	// Method to delete a pharmacy

	public void deletePharmacy(long id) {
		pharmacyRepository.deleteById(id);
	}

}
