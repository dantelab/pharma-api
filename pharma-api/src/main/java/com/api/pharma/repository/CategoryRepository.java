package com.api.pharma.repository;

import org.springframework.data.repository.CrudRepository;
import com.api.pharma.entity.Category;

public interface CategoryRepository extends CrudRepository<Category, Long> {

}
