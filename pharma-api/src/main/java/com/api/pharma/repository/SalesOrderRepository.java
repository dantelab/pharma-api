package com.api.pharma.repository;

import org.springframework.data.repository.CrudRepository;
import com.api.pharma.entity.SalesOrder;

public interface SalesOrderRepository extends CrudRepository<SalesOrder, Long> {

}
