package com.api.pharma.repository;

import org.springframework.data.repository.CrudRepository;
import com.api.pharma.entity.User;

public interface UserRepository extends CrudRepository<User, Long> {

}
