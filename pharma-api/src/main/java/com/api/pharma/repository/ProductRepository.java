package com.api.pharma.repository;

import java.util.Collection;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import com.api.pharma.entity.Product;

public interface ProductRepository extends CrudRepository<Product, Long> {

	String query = "SELECT * FROM Producto WHERE Descripcion LIKE ?1%";

	@Query(value = query, nativeQuery = true)
	Collection<Product> getProductByDescription(String name);
}
