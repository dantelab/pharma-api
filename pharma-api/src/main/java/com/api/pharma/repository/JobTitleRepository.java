package com.api.pharma.repository;

import org.springframework.data.repository.CrudRepository;
import com.api.pharma.entity.JobTitle;

public interface JobTitleRepository extends CrudRepository<JobTitle, Long> {

}
