package com.api.pharma.repository;

import org.springframework.data.repository.CrudRepository;
import com.api.pharma.entity.Laboratory;

public interface LaboratoryRepository extends CrudRepository<Laboratory, Long> {

}
