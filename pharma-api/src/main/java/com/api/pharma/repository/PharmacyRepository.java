package com.api.pharma.repository;

import org.springframework.data.repository.CrudRepository;
import com.api.pharma.entity.Pharmacy;

public interface PharmacyRepository extends CrudRepository<Pharmacy, Long> {

}
