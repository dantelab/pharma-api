package com.api.pharma.repository;

import org.springframework.data.repository.CrudRepository;
import com.api.pharma.entity.District;

public interface DistrictRepository extends CrudRepository<District, Long> {

}
