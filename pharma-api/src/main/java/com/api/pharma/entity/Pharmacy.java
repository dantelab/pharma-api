package com.api.pharma.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "Farmacia")

public class Pharmacy implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "FarmaciaId")
	private String farmaciaId;

	@Column(name = "Nombre")
	private String nombre;

	@Column(name = "Direccion")
	private String direccion;

	@Column(name = "Telefono")
	private String telefono;

	@Column(name = "Estado")
	private boolean estado;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "DistritoId")
	private District DistritoFarmacia;

	@JsonIgnore
	@OneToMany(mappedBy = "FarmaciaCliente", cascade = CascadeType.ALL)
	private List<Customer> listaCliente;

	@JsonIgnore
	@OneToMany(mappedBy = "FarmaciaOrdenVenta", cascade = CascadeType.ALL)
	private List<SalesOrder> listaOrdenVenta;

	@JsonIgnore
	@OneToMany(mappedBy = "FarmaciaUsuario", cascade = CascadeType.ALL)
	private List<User> listaUsuario;

//	@JsonIgnore
//	@OneToMany(mappedBy = "FarmaciaDetalleVenta", cascade = CascadeType.ALL)
//	private List<SalesOrderItem> listaDetalleVenta;

	// GET & SET

	public String getNombre() {
		return nombre;
	}

	public String getFarmaciaId() {
		return farmaciaId;
	}

	public void setFarmaciaId(String farmaciaId) {
		this.farmaciaId = farmaciaId;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public boolean isEstado() {
		return estado;
	}

	public void setEstado(boolean estado) {
		this.estado = estado;
	}

	public District getDistritoFarmacia() {
		return DistritoFarmacia;
	}

	public void setDistritoFarmacia(District distritoFarmacia) {
		DistritoFarmacia = distritoFarmacia;
	}

	public List<Customer> getListaCliente() {
		return listaCliente;
	}

	public void setListaCliente(List<Customer> listaCliente) {
		this.listaCliente = listaCliente;
	}

//	public List<SalesOrder> getListaOrdenVenta() {
//		return listaOrdenVenta;
//	}
//
//	public void setListaOrdenVenta(List<SalesOrder> listaOrdenVenta) {
//		this.listaOrdenVenta = listaOrdenVenta;
//	}

	public List<User> getListaUsuario() {
		return listaUsuario;
	}

	public void setListaUsuario(List<User> listaUsuario) {
		this.listaUsuario = listaUsuario;
	}

//	public List<SalesOrderItem> getListaDetalleVenta() {
//		return listaDetalleVenta;
//	}
//
//	public void setListaDetalleVenta(List<SalesOrderItem> listaDetalleVenta) {
//		this.listaDetalleVenta = listaDetalleVenta;
//	}

}
