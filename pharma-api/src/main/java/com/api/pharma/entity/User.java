package com.api.pharma.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "Usuario")

public class User implements Serializable {

	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private UserCK usuarioCK;

	@Column(name = "Correo")
	private String correo;

	@Column(name = "Clave")
	private String clave;

	@Column(name = "Nombre")
	private String nombre;

	@Column(name = "Estado")
	private boolean estado;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "CargoId")
	private JobTitle CargoUsuario;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "farmaciaId", referencedColumnName = "farmaciaId", insertable = false, updatable = false)
	private Pharmacy FarmaciaUsuario;

	@JsonIgnore
	@OneToMany(mappedBy = "UsuarioOrdenVenta")
	private List<SalesOrder> listaOrdenVenta;

	// GET & SET

	public UserCK getUsuarioCK() {
		return usuarioCK;
	}

	public void setUsuarioCK(UserCK usuarioCK) {
		this.usuarioCK = usuarioCK;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public boolean isEstado() {
		return estado;
	}

	public void setEstado(boolean estado) {
		this.estado = estado;
	}

	public JobTitle getCargoUsuario() {
		return CargoUsuario;
	}

	public void setCargoUsuario(JobTitle cargoUsuario) {
		CargoUsuario = cargoUsuario;
	}

	public Pharmacy getFarmaciaUsuario() {
		return FarmaciaUsuario;
	}

	public void setFarmaciaUsuario(Pharmacy farmaciaUsuario) {
		FarmaciaUsuario = farmaciaUsuario;
	}

	public List<SalesOrder> getListaOrdenVenta() {
		return listaOrdenVenta;
	}

	public void setListaOrdenVenta(List<SalesOrder> listaOrdenVenta) {
		this.listaOrdenVenta = listaOrdenVenta;
	}

}
