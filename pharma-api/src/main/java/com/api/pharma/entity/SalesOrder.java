package com.api.pharma.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "OrdenVenta")

public class SalesOrder implements Serializable {

	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private SalesOrderCK ordenVentaCK;
	
//	@EmbeddedId
//	private CustomerCK clienteCK;
//	
//	@EmbeddedId
//	private UserCK usuarioCK;
	
	@Column(name = "Serie")
	private String serie;

	@Column(name = "Fecha")
	@Temporal(TemporalType.DATE)
	private Date fecha;

	@Column(name = "Condicion")
	private String condicion;
	
	@JsonIgnore
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumns({@JoinColumn(name = "usuarioId", insertable = false, updatable = false),
		  @JoinColumn(name = "farmaciaId", insertable = false, updatable = false)
	})
	private User UsuarioOrdenVenta;
	
	@JsonIgnore
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumns({@JoinColumn(name = "clienteId", insertable = false, updatable = false),
		  @JoinColumn(name = "farmaciaId", insertable = false, updatable = false)
	})
	private Customer ClienteOrdenVenta;

//	@JsonIgnore
//	@ManyToOne
//	@JoinColumn(name = "UsuarioId")
//	private User UsuarioOrdenVenta;

//	@ManyToOne
//	@JoinColumns({ @JoinColumn(name = "UsuarioId", referencedColumnName = "UsuarioId"),
//			@JoinColumn(name = "FarmaciaId", referencedColumnName = "FarmaciaId") })
//	private User UsuarioOrdenVenta;

//	@JsonIgnore
//	@ManyToOne
//	@JoinColumn(name = "farmaciaId", referencedColumnName = "farmaciaId", insertable = false, updatable = false)
//	private Pharmacy FarmaciaProducto;

//	@ManyToOne
//	@JoinColumns({ @JoinColumn(name = "ClienteId", referencedColumnName = "ClienteId"),
//			@JoinColumn(name = "FarmaciaId", referencedColumnName = "FarmaciaId") })
//	private Customer ClienteOrdenVenta;

//	@JsonIgnore
//	@ManyToOne
//	@JoinColumn(name = "ClienteId", referencedColumnName = "ClienteId", insertable = false, updatable = false)
//	private Customer ClienteOrdenVenta;

//	@JsonIgnore
//	@ManyToOne
//	@JoinColumns({    
//		  @JoinColumn(name = "OrdenVentaId", referencedColumnName = "OrdenVentaId"),
//		  @JoinColumn(name = "FarmaciaId", referencedColumnName = "FarmaciaId")
//		})
//	private Customer ClienteOrdenVenta;
//	

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "farmaciaId", referencedColumnName = "farmaciaId", insertable = false, updatable = false)
	private Pharmacy FarmaciaOrdenVenta;	
	
//	@JsonIgnore
//	@ManyToOne
//	@PrimaryKeyJoinColumn(name = "UsuarioId")
//	private User UsuarioOrdenVenta;
//	
//	@JsonIgnore
//	@ManyToOne
//	@PrimaryKeyJoinColumn(name = "CustomerId")
//	private Customer ClienteOrdenVenta;

	

	// TODO: LLAVE ONE TO MANY
	public SalesOrderCK getOrdenVentaCK() {
		return ordenVentaCK;
	}

	public void setOrdenVentaCK(SalesOrderCK ordenVentaCK) {
		this.ordenVentaCK = ordenVentaCK;
	}

//	public CustomerCK getClienteCK() {
//		return clienteCK;
//	}
//
//	public void setClienteCK(CustomerCK clienteCK) {
//		this.clienteCK = clienteCK;
//	}
//
//	public UserCK getUsuarioCK() {
//		return usuarioCK;
//	}
//
//	public void setUsuarioCK(UserCK usuarioCK) {
//		this.usuarioCK = usuarioCK;
//	}

	public String getSerie() {
		return serie;
	}

	public void setSerie(String serie) {
		this.serie = serie;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getCondicion() {
		return condicion;
	}

	public void setCondicion(String condicion) {
		this.condicion = condicion;
	}

	public User getUsuarioOrdenVenta() {
		return UsuarioOrdenVenta;
	}

	public void setUsuarioOrdenVenta(User usuarioOrdenVenta) {
		UsuarioOrdenVenta = usuarioOrdenVenta;
	}

	public Customer getClienteOrdenVenta() {
		return ClienteOrdenVenta;
	}

	public void setClienteOrdenVenta(Customer clienteOrdenVenta) {
		ClienteOrdenVenta = clienteOrdenVenta;
	}

	public Pharmacy getFarmaciaOrdenVenta() {
		return FarmaciaOrdenVenta;
	}

	public void setFarmaciaOrdenVenta(Pharmacy farmaciaOrdenVenta) {
		FarmaciaOrdenVenta = farmaciaOrdenVenta;
	}
	

}
