package com.api.pharma.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

@Embeddable

public class UserCK implements Serializable {

	private static final long serialVersionUID = 1L;

	@NotNull
	@Column(name = "UsuarioId")
	private Long usuarioId;

	@NotNull
	@Column(name = "FarmaciaId")
	private String farmaciaId;

	public UserCK() {

	}

	public UserCK(@NotNull Long usuarioId, @NotNull String farmaciaId) {
		this.usuarioId = usuarioId;
		this.farmaciaId = farmaciaId;
	}

	public Long getUsuarioId() {
		return usuarioId;
	}

	public void setUsuarioId(Long usuarioId) {
		this.usuarioId = usuarioId;
	}

	public String getFarmaciaId() {
		return farmaciaId;
	}

	public void setFarmaciaId(String farmaciaId) {
		this.farmaciaId = farmaciaId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((farmaciaId == null) ? 0 : farmaciaId.hashCode());
		result = prime * result + ((usuarioId == null) ? 0 : usuarioId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserCK other = (UserCK) obj;
		if (farmaciaId == null) {
			if (other.farmaciaId != null)
				return false;
		} else if (!farmaciaId.equals(other.farmaciaId))
			return false;
		if (usuarioId == null) {
			if (other.usuarioId != null)
				return false;
		} else if (!usuarioId.equals(other.usuarioId))
			return false;
		return true;
	}

}
