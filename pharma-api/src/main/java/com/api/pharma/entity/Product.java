package com.api.pharma.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "Producto")

public class Product implements Serializable {

	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private ProductCK productoCK;

	@Column(name = "Descripcion")
	private String descripcion;

	@Column(name = "Precio")
	private double precio;

	@Column(name = "Stock")
	private int stock;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "CategoriaId")
	private Category CategoriaProducto;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "LaboratorioId")
	private Laboratory LaboratorioProducto;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "farmaciaId", referencedColumnName = "farmaciaId", insertable = false, updatable = false)
	private Pharmacy FarmaciaProducto;

//	@JsonIgnore
//	@OneToMany(mappedBy = "ProductoDetalleVenta")
//	private List<SalesOrderItem> listaDetalleVenta;

	// GET & SET

	public ProductCK getProductoCK() {
		return productoCK;
	}

	public void setProductoCK(ProductCK productoCK) {
		this.productoCK = productoCK;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	public Category getCategoriaProducto() {
		return CategoriaProducto;
	}

	public void setCategoriaProducto(Category categoriaProducto) {
		CategoriaProducto = categoriaProducto;
	}

	public Laboratory getLaboratorioProducto() {
		return LaboratorioProducto;
	}

	public void setLaboratorioProducto(Laboratory laboratorioProducto) {
		LaboratorioProducto = laboratorioProducto;
	}

	public Pharmacy getFarmaciaProducto() {
		return FarmaciaProducto;
	}

	public void setFarmaciaProducto(Pharmacy farmaciaProducto) {
		FarmaciaProducto = farmaciaProducto;
	}

//	public List<SalesOrderItem> getListaDetalleVenta() {
//		return listaDetalleVenta;
//	}
//
//	public void setListaDetalleVenta(List<SalesOrderItem> listaDetalleVenta) {
//		this.listaDetalleVenta = listaDetalleVenta;
//	}

}
