package com.api.pharma.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "Laboratorio")

public class Laboratory implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "LaboratorioId")
	private Long laboratorioId;

	@Column(name = "Nombre")
	private String nombre;

	@JsonIgnore
	@OneToMany(mappedBy = "LaboratorioProducto")
	private List<Product> listaProducto;

	// GET & SET

	public Long getLaboratorioId() {
		return laboratorioId;
	}

	public void setLaboratorioId(Long laboratorioId) {
		this.laboratorioId = laboratorioId;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<Product> getListaProducto() {
		return listaProducto;
	}

	public void setListaProducto(List<Product> listaProducto) {
		this.listaProducto = listaProducto;
	}

}
