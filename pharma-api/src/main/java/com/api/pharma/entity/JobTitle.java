package com.api.pharma.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "Cargo")

public class JobTitle implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "CargoId")
	private Long cargoId;

	@Column(name = "Nombre")
	private String nombre;

	@JsonIgnore
	@OneToMany(mappedBy = "CargoUsuario")
	private List<User> listaUsuario;
	
	// GET & SET

	public Long getCargoId() {
		return cargoId;
	}

	public void setCargoId(Long cargoId) {
		this.cargoId = cargoId;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<User> getListaUsuario() {
		return listaUsuario;
	}

	public void setListaUsuario(List<User> listaUsuario) {
		this.listaUsuario = listaUsuario;
	}

}
