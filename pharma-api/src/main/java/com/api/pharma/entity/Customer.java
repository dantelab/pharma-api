package com.api.pharma.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "Cliente")

public class Customer implements Serializable {

	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private CustomerCK clienteCK;

	@Column(name = "Nombre")
	private String nombre;

	@Column(name = "ApellidoPaterno")
	private String apellidoPaterno;

	@Column(name = "ApellidoMaterno")
	private String apellidoMaterno;

	@Column(name = "Sexo")
	private String sexo;

	@Column(name = "Dni")
	private String dni;

	@Column(name = "Ruc")
	private String ruc;

	@Column(name = "FechaRegistro")
	@Temporal(TemporalType.DATE)
	private Date fechaRegistro;

	@Column(name = "Tipo")
	private String tipo;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "DistritoId")
	private District DistritoCliente;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "farmaciaId", referencedColumnName = "farmaciaId", insertable = false, updatable = false)
	private Pharmacy FarmaciaCliente;

	@JsonIgnore
	@OneToMany(mappedBy = "ClienteOrdenVenta")
	private List<SalesOrder> listaOrdenVenta;

	// GET & SET

	public CustomerCK getClienteCK() {
		return clienteCK;
	}

	public void setClienteCK(CustomerCK clienteCK) {
		this.clienteCK = clienteCK;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidoPaterno() {
		return apellidoPaterno;
	}

	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}

	public String getApellidoMaterno() {
		return apellidoMaterno;
	}

	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getRuc() {
		return ruc;
	}

	public void setRuc(String ruc) {
		this.ruc = ruc;
	}

	public Date getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public District getDistritoCliente() {
		return DistritoCliente;
	}

	public void setDistritoCliente(District distritoCliente) {
		DistritoCliente = distritoCliente;
	}

	public Pharmacy getFarmaciaCliente() {
		return FarmaciaCliente;
	}

	public void setFarmaciaCliente(Pharmacy farmaciaCliente) {
		FarmaciaCliente = farmaciaCliente;
	}

//	public List<SalesOrder> getListaOrdenVenta() {
//		return listaOrdenVenta;
//	}
//
//	public void setListaOrdenVenta(List<SalesOrder> listaOrdenVenta) {
//		this.listaOrdenVenta = listaOrdenVenta;
//	}

}
