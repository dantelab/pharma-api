package com.api.pharma.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

@Embeddable

public class SalesOrderCK implements Serializable {

	private static final long serialVersionUID = 1L;

	@NotNull
	@Column(name = "OrdenVentaId")
	private Long ordenVentaId;

	@NotNull
	@Column(name = "FarmaciaId")
	private String farmaciaId;

	public SalesOrderCK() {

	}

	public SalesOrderCK(@NotNull Long ordenVentaId, @NotNull String farmaciaId) {
		this.ordenVentaId = ordenVentaId;
		this.farmaciaId = farmaciaId;
	}

	public Long getOrdenVentaId() {
		return ordenVentaId;
	}

	public void setOrdenVentaId(Long ordenVentaId) {
		this.ordenVentaId = ordenVentaId;
	}

	public String getFarmaciaId() {
		return farmaciaId;
	}

	public void setFarmaciaId(String farmaciaId) {
		this.farmaciaId = farmaciaId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((farmaciaId == null) ? 0 : farmaciaId.hashCode());
		result = prime * result + ((ordenVentaId == null) ? 0 : ordenVentaId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SalesOrderCK other = (SalesOrderCK) obj;
		if (farmaciaId == null) {
			if (other.farmaciaId != null)
				return false;
		} else if (!farmaciaId.equals(other.farmaciaId))
			return false;
		if (ordenVentaId == null) {
			if (other.ordenVentaId != null)
				return false;
		} else if (!ordenVentaId.equals(other.ordenVentaId))
			return false;
		return true;
	}

}
