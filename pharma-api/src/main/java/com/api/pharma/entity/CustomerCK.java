package com.api.pharma.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

@Embeddable

public class CustomerCK implements Serializable {

	private static final long serialVersionUID = 1L;

	@NotNull
	@Column(name = "ClienteId")
	private Long clienteId;

	@NotNull
	@Column(name = "FarmaciaId")
	private String farmaciaId;

	public CustomerCK() {

	}

	public CustomerCK(@NotNull Long clienteId, @NotNull String farmaciaId) {
		this.clienteId = clienteId;
		this.farmaciaId = farmaciaId;
	}

	public Long getClienteId() {
		return clienteId;
	}

	public void setClienteId(Long clienteId) {
		this.clienteId = clienteId;
	}

	public String getFarmaciaId() {
		return farmaciaId;
	}

	public void setFarmaciaId(String farmaciaId) {
		this.farmaciaId = farmaciaId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((clienteId == null) ? 0 : clienteId.hashCode());
		result = prime * result + ((farmaciaId == null) ? 0 : farmaciaId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CustomerCK other = (CustomerCK) obj;
		if (clienteId == null) {
			if (other.clienteId != null)
				return false;
		} else if (!clienteId.equals(other.clienteId))
			return false;
		if (farmaciaId == null) {
			if (other.farmaciaId != null)
				return false;
		} else if (!farmaciaId.equals(other.farmaciaId))
			return false;
		return true;
	}

}
