package com.api.pharma.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "Distrito")

public class District implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "DistritoId")
	private Long distritoId;

	@Column(name = "Nombre")
	private String nombre;

	@JsonIgnore
	@OneToMany(mappedBy = "DistritoCliente")
	private List<Customer> listaCliente;

	@JsonIgnore
	@OneToMany(mappedBy = "DistritoFarmacia")
	private List<Pharmacy> listaFarmacia;

	// GET & SET

	public Long getDistritoId() {
		return distritoId;
	}

	public void setDistritoId(Long distritoId) {
		this.distritoId = distritoId;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<Customer> getListaCliente() {
		return listaCliente;
	}

	public void setListaCliente(List<Customer> listaCliente) {
		this.listaCliente = listaCliente;
	}

	public List<Pharmacy> getListaFarmacia() {
		return listaFarmacia;
	}

	public void setListaFarmacia(List<Pharmacy> listaFarmacia) {
		this.listaFarmacia = listaFarmacia;
	}

}
