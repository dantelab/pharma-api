package com.api.pharma.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

@Embeddable

public class ProductCK implements Serializable {

	private static final long serialVersionUID = 1L;

	@NotNull
	@Column(name = "ProductoId")
	private Long productoId;

	@NotNull
	@Column(name = "FarmaciaId")
	private String farmaciaId;

	public ProductCK() {

	}

	public ProductCK(@NotNull Long productoId, @NotNull String farmaciaId) {
		this.productoId = productoId;
		this.farmaciaId = farmaciaId;
	}

	public Long getProductoId() {
		return productoId;
	}

	public void setProductoId(Long productoId) {
		this.productoId = productoId;
	}

	public String getFarmaciaId() {
		return farmaciaId;
	}

	public void setFarmaciaId(String farmaciaId) {
		this.farmaciaId = farmaciaId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((farmaciaId == null) ? 0 : farmaciaId.hashCode());
		result = prime * result + ((productoId == null) ? 0 : productoId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProductCK other = (ProductCK) obj;
		if (farmaciaId == null) {
			if (other.farmaciaId != null)
				return false;
		} else if (!farmaciaId.equals(other.farmaciaId))
			return false;
		if (productoId == null) {
			if (other.productoId != null)
				return false;
		} else if (!productoId.equals(other.productoId))
			return false;
		return true;
	}

}
